def is_triangle(a, b, c):
    flag = True
    if a + b <= c:
        flag = False

    if a + c <= b:
        flag = False

    if b + c <= a:
        flag = False
    return flag
